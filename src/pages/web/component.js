import React, { useState, useEffect } from "react";
import { Progress } from "reactstrap";
import "./style.css";
import withWidth from "@material-ui/core/withWidth";
import CircularProgress from '@material-ui/core/CircularProgress';
import IconButton from "@material-ui/core/IconButton";
import ClearIcon from '@material-ui/icons/Clear';
import DoneIcon from '@material-ui/icons/Done';

import CryptoJS from "crypto-js/sha256";
import WebFrame from '../../components/web-loading/web.loading.component';
import ModalForm from '../../components/web-modal/web.modal.create';

import { connect } from 'react-redux';
import {
  setUserInitialData,
  setListKegiataIntialData,
  checkIsYesterdayDataIsEmpty,
} from "../../redux/general/action";

let date = new Date().toLocaleDateString().split('/').join('-');
const equation = "inc";
const App = props => {
  const { width, history, currentUser, setUserInitialData, setListKegiataIntialData, checkIsYesterdayDataIsEmpty, listType } = props;
  const [incomes, setIncomes] = useState(0);
  const [expenses, setExpenses] = useState(0);
  const [incomePercentage, setIncomesPercentage] = useState(0);
  const [expensePercentage, setExpensePercentage] = useState(0);
  const [total, setTotal] = useState("");

  const [listSkillTarget, setListSkillTarget] = useState([]);
  const [selectedSkill, setSelectedSkill] = useState("");

  const [kegiatan, setKegiatan] = useState("");
  const [listKegiatan, setListKegiatan] = useState([]);
  const [totalHours, setTotalHours] = useState("");
  const [isDataLoading, setIsDataLoading] = useState(true);


  // INIT USERS DATA
  useEffect(() => {
    if (currentUser && selectedSkill) {
      setIsDataLoading(true);
      const { skillName, skillHoursTarget } = selectedSkill;
      let newListData = [], newExpenses = 0, newIncomes = 0;
      setUserInitialData(currentUser, skillName, skillHoursTarget, null).then(
        res => {
          if (res) {
            newListData = res.listExpenses;
            newExpenses = res.expenses;
            newIncomes = res.incomes;
            setExpenses(newExpenses);
            setIncomes(newIncomes);
            setListKegiatan(newListData);
          }
        }
      );
      setListKegiataIntialData(currentUser, skillName, date, null).then(res => {
        if (res.data) {
          newListData = [...newListData, ...res.data];
          setListKegiatan([...newListData,...res.data]);
        }
      });
      checkIsYesterdayDataIsEmpty(currentUser, skillName, skillHoursTarget)
        .then(res => {
          newListData = [...newListData, ...res.listExpenses];
          newIncomes = newIncomes + res.incomes;
          newExpenses = newExpenses + res.expenses;
          setExpenses(newExpenses);
          setIncomes(newIncomes);
          setListKegiatan(newListData);
        })
      
      setTimeout(() => {
        setIsDataLoading(false);
      }, 1000);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [listType, selectedSkill, listSkillTarget, currentUser, setUserInitialData, setListKegiataIntialData, checkIsYesterdayDataIsEmpty]);

  // INIT LIST TYPE
  useEffect(() => {
    if(listType.length > 0 && currentUser) {
      setListSkillTarget(listType);
      setSelectedSkill(listType[0])
    }
  }, [listType, currentUser])

  // CHECK SCREEN SIZE
  useEffect(() => {
    if(width === 'xs') history.push('/mobile')
  }, [width, history])

  useEffect(() => {
    const { skillHoursTarget } = selectedSkill;
    const processTotal = () => {
      let margin = incomes - expenses;
      if (margin <= 0) setTotal("0%");
      else {
        let total = (100 / parseInt(skillHoursTarget)) * margin;
        setTotal(`${processNumber(total)}%`);
      }
    };
    const processPercentage = () => {
      let parseIncome = parseInt(incomes);
      let parseExpense = parseInt(expenses);
      let total = parseIncome + parseExpense;
      let percentIncome = (100 / total) * parseIncome;
      let percentExpense = (100 / total) * parseExpense;
      setIncomesPercentage(processNumber(percentIncome));
      setExpensePercentage(processNumber(percentExpense));
    };
    processTotal();
    processPercentage();
  }, [incomes, expenses, selectedSkill]);

  // PROCESS NUMBER
  const processNumber = num => {
    let length = `${num}`.length;
    if (length > 5) num = `${num}`.slice(0, 4);
    return num;
  };

  // SUBMIT JAM
  const submitJam = totalHours => {
    const { skillName, skillHoursTarget } = selectedSkill
    let newExpenses = 0, newIncomes = 0;
    if (equation === "inc" && totalHours > 0) {
      newIncomes = parseInt(incomes) + totalHours;
      setIncomes(newIncomes);
    }
    if (incomes !== 0 && totalHours > 0) {
      if (equation === "exp") {
        newExpenses = parseInt(expenses) + totalHours;
        newIncomes = parseInt(incomes) - totalHours;
        if (parseInt(incomes) < totalHours && parseInt(incomes) - totalHours <= 0) {
          newExpenses = 0;
          newIncomes = 0;
        }
        setExpenses(newExpenses);
        setIncomes(newIncomes)
      }
    }
    setUserInitialData(currentUser, skillName, skillHoursTarget, {incomes:newIncomes, expenses: newExpenses});
  };
  // SUBMIT LIST KEGIATAN
  const submitListKegiatan = total => {
    const { skillName } = selectedSkill;
    let newKegiatanData = {};
    let newListKegiatan = [];

    newKegiatanData["kegiatan"] = kegiatan;
    newKegiatanData["type"] = equation;
    newKegiatanData["hours"] = total;
    newKegiatanData["percentage"] = processNumber(
      (100 / parseInt(incomes)) * total
    );
    newKegiatanData["id"] = CryptoJS(
      JSON.stringify({ ...newKegiatanData, no: listKegiatan.length + 1 })
    ).toString();
    newListKegiatan.push(newKegiatanData);
    newListKegiatan = [...listKegiatan, ...newListKegiatan];
    if (incomes === 0 && equation === "exp" && incomes - total <= 0) {
      newListKegiatan = [];
    }
    setListKegiatan(newListKegiatan);
    setListKegiataIntialData(currentUser, skillName, date, newListKegiatan);
  };
  // HANDLE SUBMIT
  const handleSubmit = e => {
    e.preventDefault();
    let hours = parseInt(totalHours);
    if (hours > 0 && kegiatan.length > 0) {
      submitListKegiatan(hours);
      submitJam(hours);
    }
    setKegiatan("");
    setTotalHours("");
  };

  // HANDLE DELETE
  const handleDeleteListItem = (equation, data, id) => {
    const { skillName, skillHoursTarget } = selectedSkill;
    let hours = data.hours;
    let newListItem = listKegiatan;
    let value = 0;
    newListItem = newListItem.filter(item => item.id !== id);

    if (equation === "inc") {
      value = parseInt(incomes) - hours;
      if(value < 0) value = 0;
      setIncomes(value)
      setUserInitialData(currentUser, skillName, skillHoursTarget, {incomes:value, expenses});
    } 
    setListKegiatan(newListItem);
    setListKegiataIntialData(currentUser, skillName, date, newListItem);
  };

  // RENDER LIST KEGIATAN
  const renderKegiatan = (equation = "inc") => {
    let newList = listKegiatan.filter(item => item.type === equation);
    return newList.map(item => {
      return (
        <div className="item clearfix" id="expense-0" key={item.id}>
          <div className="item__description">{item.kegiatan}</div>
          <div className="right clearfix">
            <div className="item__value">
              {item.type === "inc" ? "+" : "-"} {item.hours}
            </div>
            {item.type === "exp" && (
              <div className="item__percentage">{item.percentage}%</div>
            )}
            {equation === 'inc' &&  
              <IconButton className="item__delete item__delete--btn" size="small" style={{ color: '#28B9B5' }} onClick={() => handleDeleteListItem(equation, item, item.id)}>
                <ClearIcon fontSize="small" />
              </IconButton> 
            }
          </div>
        </div>
      );
    });
  };

  // RENDER SELECT SKILL TYPE OPTIONS
  const renderSelectSkill = () => {
    return listSkillTarget.map((item, key) => {
      return <option value={JSON.stringify(item)} key={`${item}/${key}`}>{item.skillName}</option>
    })
  }

  return (
    <WebFrame>
      <ModalForm userId={currentUser} />
      <div className="top">
        <div className="budget">
          <div className="budget__title">
            {parseInt(incomes) < parseInt(selectedSkill.skillHoursTarget) ? (
              <span className="budget__title--month">
                {parseInt(selectedSkill.skillHoursTarget) > 1000 ? "Progress to be uncommon ": "Progress to be ready for uncommon journey "} is {total}
              </span>
            ) : (
                parseInt(selectedSkill.skillHoursTarget) > 1000 ? "YOU ARE UNCOMMON NOW !" : "YOU ARE READY FOR UNCOMMON JOURNEY "
          
        )}
          </div>
          <div className="budget__value">
            <Progress striped value={parseInt(total)}>
              {incomes}/ {selectedSkill.skillHoursTarget}
            </Progress>
          </div>
          <div className="budget__income clearfix">
            <div className="budget__income--text">Hours Incomes</div>
            <div className="right">
              <div className="budget__income--value">+ {incomes}</div>
              <div className="budget__income--percentage">
                {incomePercentage || 0}%
              </div>
            </div>
          </div>
          <div className="budget__expenses clearfix">
            <div className="budget__expenses--text">Hours Expenses</div>
            <div className="right clearfix">
              <div className="budget__expenses--value">- {expenses}</div>
              <div className="budget__expenses--percentage">
                {expensePercentage || 0}%
              </div>
            </div>
          </div>
        </div>
      </div>
 
      <div className="bottom">
        <div className="add">
          <div className="add__container">
            <select
              className="add__description"
              onChange={e => setSelectedSkill(JSON.parse(e.target.value))}
              options
            >
              {renderSelectSkill()}
            </select>
            <form onSubmit={handleSubmit} style={{ display: "inline" }}>
              <input
                type="text"
                className="add__description"
                placeholder="Kegiatan"
                value={kegiatan}
                onChange={e => setKegiatan(e.target.value)}
              />
              <input
                type="number"
                className="add__value"
                placeholder="Jam"
                value={totalHours}
                onChange={e => setTotalHours(e.target.value)}
              />
              <IconButton type="submit" style={{ color:'#28B9B5'}}>
                <DoneIcon/>
              </IconButton> 
            </form>
          </div>
        </div>
        {isDataLoading ? <div
        className="mt-5"
        style={{
          display: "flex",
          width: "100%",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <CircularProgress
          style={{ marginRight: "10px" }}
          size={23}
          color="inherit"
        />
        <h4>Loading Data. . .</h4>
      </div> : listKegiatan.length !== 0 ? (
          <div className="container clearfix">
            <div className="income">
              <h2 className="icome__title">Hours Income</h2>
              <div className="income__list">{renderKegiatan("inc")}</div>
            </div>
            <div className="expenses">
              <h2 className="expenses__title">Hours Expenses</h2>
              <div className="expenses__list">{renderKegiatan("exp")}</div>
            </div>
          </div>
        ) : (
          <h1 className="text-center mt-5">No Data</h1>
        )
      }
      </div>
    </WebFrame>
  );
};

const mapStateToProps = state => ({...state.AuthReducer, ...state.GeneralReducer });
export default connect(mapStateToProps, {
  setUserInitialData,
  setListKegiataIntialData,
  checkIsYesterdayDataIsEmpty,
})(withWidth()(App));
