import { makeStyles } from "@material-ui/core/styles";
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  rootBottomNav: {
    width: '100%',
    position:'fixed',
    bottom: 0,
    borderTop: '1px solid grey'
  },
  frontContent: {
    padding:'20px',
    paddingTop:'30px',
    paddingBottom:'10px',
    height:'100%',
    margin:'0em',
    overflowY:'auto'
  },
  paperProgress: {
    height:'170px',
    width:'100%',
    marginBottom: '30px',
  },
  paperProgressHeader:{
    padding: '10px 10px 0px 15px',
  },
  paperProgressContent:{
    padding: '0 10px 0px 15px',
    marginBottom:'10px',
  },
  title: {
    flexGrow: 1
  }
}));

export default useStyles;