import React, { useEffect } from 'react';
import withWidth from "@material-ui/core/withWidth";
import useStyles from './styles';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Paper from '@material-ui/core/Paper';
import Chip from '@material-ui/core/Chip';
import Container from '@material-ui/core/Container';
import IconButton from '@material-ui/core/IconButton';
import VisibilityIcon from '@material-ui/icons/Visibility';

import AddCircleIcon from '@material-ui/icons/AddCircle';
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';

import { Progress } from "reactstrap";

import ModalFormCreateButton from '../../components/mobile-modal/mobile.modal.create';

import { connect } from 'react-redux';
import {
  setUserInitialData,
  setListKegiataIntialData,
  checkIsYesterdayDataIsEmpty,
} from "../../redux/general/action";

// let date = new Date().toLocaleDateString().split('/').join('-');
// const equation = "inc";
const MobileUI = (props) => {
  // const { width, history, listType, currentUser } = props;
  const { width, history } = props;

  // const [listSkillTarget, setListSkillTarget] = useState([]);
  // const [selectedSkill, setSelectedSkill] = useState("");
  // const [navValue, setNavValue] = useState(0);

  useEffect(() => {
    if(width !== 'xs') history.push('/')
  }, [width, history])


  // INIT LIST TYPE
  // useEffect(() => {
  //   if (listType.length > 0 && currentUser) {
  //     setListSkillTarget(listType);
  //     setSelectedSkill(listType[0])
  //   }
  // }, [listType, currentUser])

  const classes = useStyles();
  return (
    <div style={{ height: '100vh', width: '100vw', overflowX: 'hidden', padding:'40px 0 0px 0'}}>
      <div className={classes.root}>
        <AppBar position="fixed">
          <Toolbar>
            <Typography variant="h6" className={classes.title}>
              ---- UNDER DEVELOPMENT  -----
            </Typography>
            {/* <ModalFormCreateButton anyComponent={false} title="Add new skill" type="skill" /> */}
          </Toolbar>
        </AppBar>
      </div>
      <Container className={classes.frontContent}>
        <Paper elevation={3} className={classes.paperProgress} onClick={() => console.log('hello there')}>
          <div className={classes.paperProgressHeader}>
            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
              <Typography style={{ display: 'flex', alignItems: 'center', width: '300px' }}>
                <span className="mr-2">Marathon - 100%</span>
                <Chip style={{ marginLeft: '10px' }} size="small" label="Today Activity : 0" />
              </Typography>
              <IconButton size="small">
                <VisibilityIcon />
              </IconButton>
            </div>
            <Progress striped value={10} className="mt-2" />
          </div>
          <hr />
          <div className={classes.paperProgressContent}>
            <Chip className="mb-1" size="small" color="primary" icon={<AddCircleIcon />} label="Hours Incomes - 100%" /> <br />
            <Chip size="small" color="secondary" icon={<RemoveCircleIcon />} label="Hours Expenses - 100%" />
            <ModalFormCreateButton anyComponent={true} title="Add new activity" type="activity" />
          </div>
        </Paper>

      </Container>
      {/* <BottomNavigation
        value={navValue}
        onChange={(event, newValue) => {
          setNavValue(newValue);
        }}
        showLabels
        className={classes.rootBottomNav}
        
      >
        <BottomNavigationAction label="Recents" icon={<RestoreIcon />} />
        <BottomNavigationAction label="Favorites" icon={<FavoriteIcon />} />
        <BottomNavigationAction label="Nearby" icon={<LocationOnIcon />} />
      </BottomNavigation> */}
    </div>
  );
}

const mapStateToProps = state => ({ ...state.AuthReducer, ...state.GeneralReducer });
export default connect(mapStateToProps, {
  setUserInitialData,
  setListKegiataIntialData,
  checkIsYesterdayDataIsEmpty,
})(withWidth()(MobileUI));
