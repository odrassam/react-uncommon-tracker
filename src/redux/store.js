import { createStore, applyMiddleware } from "redux";
import rootReducer from "./root.reducer";
import { composeWithDevTools } from "redux-devtools-extension";
import reduxThunk from "redux-thunk";
 
const middlewares = [reduxThunk];
const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(...middlewares))
);

export default store;
