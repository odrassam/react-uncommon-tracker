import { combineReducers } from "redux";

import AuthReducer from './auth/reducer';
import GeneralReducer from './general/reducer';

const appReducer = combineReducers({
  AuthReducer,
  GeneralReducer,
});


export default appReducer;