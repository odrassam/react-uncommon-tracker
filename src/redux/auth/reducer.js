import Types from './types';

const initialStateUser = {
  currentUser: "",
  isLoading: true
}

const authReducer = (state = initialStateUser, action) => {
  switch (action.type) {
    case Types.IS_LOGGED_IN:
      return {
        ...state,
        isLoading: action.payload.isLoading,
        currentUser: action.payload.currentUser
      }
  
    default:
      return state;
  }
}

export default authReducer;