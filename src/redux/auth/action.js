import Types from './types';

export const checkLogin = data => ({
  type: Types.IS_LOGGED_IN,
  payload: data
})