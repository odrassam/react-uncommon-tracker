import Types from './types';

const initialState = {
  listType: [],
};

const GeneralReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.SET_USER_DATA:
      return {
        ...state,
        initialData: action.payload
      }
    case Types.SET_USER_LIST_SKILL:
      return {
        ...state,
        listType: action.payload
      }
    default:
      return state;
  }
}

export default GeneralReducer;