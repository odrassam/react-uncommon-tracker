import Types from './types'
import 'firebase/firestore';
import firebase from '../../firebase';
import CryptoJS from "crypto-js/sha256";

const setUserData = data => ({
  type: Types.SET_USER_DATA,
  payload: data,
})

const setUserListSKills = data => ({
  type: Types.SET_USER_LIST_SKILL,
  payload: data
})

const db = firebase.firestore();
  const processNumber = num => {
    let length = `${num}`.length;
    if (length > 5) num = `${num}`.slice(0, 4);
    return num;
  };

export const setUsersTypeSkill = (userId, payload) => async (dispatch, getState) => {
  const listUsersTypeSkill = db.doc(`UsersSkillLists/${userId}`);
  let newListSkills = [];
  let newSkill = {};
  
  if(payload){
      newSkill["skillName"] = payload.skillName;
      newSkill["skillHoursTarget"] = payload.skillHoursTarget;
      newListSkills.push(newSkill);
      try {
        const data = await listUsersTypeSkill.get();
        if (data.exists) {
          newListSkills = [...data.data().listType, ...newListSkills];
        }
        try {
          await listUsersTypeSkill.set({ userId, listType: newListSkills });
        } catch (e) {
          console.log(e.message);
        }
      } catch (e) {
        console.log(e.message);
      }
  } else {
    try {
      const data = await listUsersTypeSkill.get();
      if(data.exists) newListSkills = data.data().listType;
    } catch(e){
      console.log(e.message)
    }
  }
  newListSkills.reverse();
  dispatch(setUserListSKills(newListSkills));
  return newListSkills;
}

export const checkIsYesterdayDataIsEmpty = (userId, type, skillHoursTarget = 0) => async dispatch => {
  let yesterDayDate = new Date(Date.now() - 864e5).toLocaleDateString().split("/").join("-");
  const listKegiatanRef = db.doc(`ListKegiatan/${userId}/${type}/${yesterDayDate}`);
  const typeRef = db.collection(type).doc(userId);
  const skillHours = parseInt(skillHoursTarget)

  let isEmptyYesterday = false;
  let expensesAmount = 6;
  let newKegiatanData = {};
  let newListKegiatan = [];
  let newPayload = {
    incomes: 0,
    expenses: 0,
    listExpenses: [],
  };

  if(skilLHours <= 30) expensesAmount = 1;
  else if (skilLHours > 30) expensesAmount = 2;
  else if (skillHours > 100) expensesAmount = 3;
  else if (skilLHours > 500) expensesAmount = 4;
  else if (skillHours > 1000) expensesAmount = 5;
  else if (skilLHours > 2000) expensesAmount = 6;

  try {
    const data = await listKegiatanRef.get();
    if (data.exists) {
      if(data.data().isChecked === true) isEmptyYesterday = false;
      else if(data.data().data.length === 0) isEmptyYesterday = true;
      try {
        await listKegiatanRef.set({ ...data.data(), isChecked: true });
      } catch (e) {
        console.log(e.message);
      }

    }
  } catch(e){
    console.log(e.message)
  }
  
  if(isEmptyYesterday){
    try {
      const data = await typeRef.get();
      if (data.exists) {
            const dataPayload = data.data();
            newKegiatanData["kegiatan"] = "Menjadi Bajingan Pemalas -6h ";
            newKegiatanData["type"] = "exp";
            newKegiatanData["hours"] = expensesAmount;
            newKegiatanData["percentage"] = processNumber(
              (100 / parseInt(dataPayload.incomes)) * newKegiatanData["hours"]
            );
            newKegiatanData["id"] = CryptoJS(
              JSON.stringify({
                ...newKegiatanData,
                no: dataPayload.listExpenses.length + 1
              })
            ).toString();
            if (dataPayload.incomes === 0) newKegiatanData["percentage"] = 100;
            newListKegiatan.push(newKegiatanData);
            newListKegiatan = [...dataPayload.listExpenses, ...newListKegiatan]
            newPayload = { ...dataPayload, incomes: dataPayload.incomes - expensesAmount ,expenses: dataPayload.expenses + expensesAmount, listExpenses:newListKegiatan };
        try {
          await typeRef.set(newPayload);
        } catch (e) {
          console.log(e.message);
        }
      }
    } catch(e){
      console.log(e.message)
    }
  }
  return newPayload
}

export const setListKegiataIntialData = (userId, type, date, payload) => async dispatch => {
  const listKegiatanRef = db.doc(`ListKegiatan/${userId}/${type}/${date}`);
  let result = [];
  let templatePayload = {
    userId, 
    data: result,
    isChecked: false,
  }

  if(payload){
    try{
      const data = await listKegiatanRef.get();
      if (data.exists) {
        try {
          await listKegiatanRef.set({ ...data.data(), data: payload });
        } catch (e) {
          console.log(e.message);
        }
      }
    } catch(e){
      console.log(e.message)
    }
    
  } else {
    try {
      const getData = await listKegiatanRef.get();
      if(!getData.exists){
        await listKegiatanRef.set(templatePayload);
      } else {
        result = getData.data();
      }
    } catch(e){
      console.log(e.message)
    }
  }

  return result;
}

export const setUserInitialData = (userId, type, target, payload) => async dispatch => {
  const typeRef = db.collection(type).doc(userId);
  let newData = {
    incomes: 0,
    expenses: 0,
    listExpenses: [],
    target,
    type,
  };
  if(payload){
    try {
      await typeRef.set({...newData, ...payload});
      dispatch(setUserData(payload))
    } catch(e){
      console.log(e.message)
    }
  } else {
    try {
      const getData = await typeRef.get();
      newData['target'] = target
      if(!getData.exists) {
        await typeRef.set(newData)
      } else {
        newData = getData.data();
      }
      dispatch(setUserData(newData))
    } catch(e){
      console.log(e.message)
    }
  }
  return newData;
}