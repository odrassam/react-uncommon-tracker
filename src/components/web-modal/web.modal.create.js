import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import TextField from "@material-ui/core/TextField";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import { makeStyles } from "@material-ui/core/styles";

import { connect } from "react-redux";
import { setUsersTypeSkill } from "../../redux/general/action";
import {  checkLogin } from "../../redux/auth/action";

const useStyles = makeStyles(theme => ({
  fab: {
    position: "absolute",
    bottom: theme.spacing(2),
    right: theme.spacing(2)
  },
  dialogAction: {
    paddingRight: "20px",
    paddingBottom: "15px"
  }
}));
const AlertDialog = props => {
  const { setUsersTypeSkill, userId, showButton = true, checkLogin, General: { listType = [] } } = props;
  const [open, setOpen] = React.useState(false);
  const [skillName, setSkillName] = React.useState("");
  const [skillHoursTarget, setSkillHoursTarget] = React.useState("");

  const classes = useStyles();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setSkillName("");
    setSkillHoursTarget("");
  };

  const checkData = () => {
    let status = listType.find(item => item.skillName === skillName)
    return status ? true : false;
  }

  const handleSubmit = e => {
    e.preventDefault();
    if (userId) {
      if(!checkData()) {
        setUsersTypeSkill(userId, { skillName, skillHoursTarget }).then(
          () => {
            checkLogin({ isLoading: false, currentUser: userId });
          }
        );
      }
    }
    handleClose();
  };

  return (
    <div>
      {showButton && (
        <Fab
          color="primary"
          aria-label="add"
          className={classes.fab}
          onClick={handleClickOpen}
        >
          <AddIcon />
        </Fab>
      )}
      <Dialog
        open={showButton ? open : true}
        onClose={showButton ? handleClose : () => {}}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <form onSubmit={handleSubmit}>
          <DialogTitle id="alert-dialog-title">Add new skills</DialogTitle>
          <DialogContent>
            <TextField
              onChange={e => setSkillName(e.target.value)}
              className="mb-3"
              label="Skill name"
              type="text"
              fullWidth
              required
            />
            <TextField
              onChange={e => setSkillHoursTarget(e.target.value)}
              className="mb-3"
              label="Target"
              fullWidth
              type="number"
              required
            />
          </DialogContent>
          <DialogActions className={classes.dialogAction}>
            {showButton && (
              <Button onClick={handleClose} color="primary">
                Cancel
              </Button>
            )}
            <Button type="submit" color="primary" autoFocus variant="contained">
              Submit
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </div>
  );
};

const mapStateToProps = state => ({ General: state.GeneralReducer })
export default connect(mapStateToProps, { setUsersTypeSkill, checkLogin })(AlertDialog);
