import React from 'react';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';
import webLoading from './web.loading.svg';
import WebModalForm from '../web-modal/web.modal.create';
import { connect } from 'react-redux';  

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#000',
    backgroundColor: 'white',
    display:'flex',
    flexDirection:'column',
    transition: 'all .2s'
  },
  loadingText: {
    display:'flex',
    width:'100%',
    justifyContent:'center',
    alignItems:'center'
  }
}));

function SimpleBackdrop(props) {
  const { children, isLoading, currentUser } = props;
  const classes = useStyles();
  return isLoading ? (
    <div>
      {currentUser && <WebModalForm showButton={false} userId={currentUser} />}
      <Backdrop className={classes.backdrop} open={true}>
        <img src={webLoading} alt="Loading logo" style={{ maxWidth: "80vw" }} />
        <div className={classes.loadingText}>
          <CircularProgress
            style={{ marginRight: "10px" }}
            size={23}
            color="inherit"
          />
          <h4>Preparing App. . .</h4>
        </div>
      </Backdrop>
    </div>
  ) : (
    <React.Fragment>{children}</React.Fragment>
  );
}

const mapStateToProps = state => state.AuthReducer
export default connect(mapStateToProps)(SimpleBackdrop)