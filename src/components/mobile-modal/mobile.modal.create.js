import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Fab from '@material-ui/core/Fab';
import TextField from '@material-ui/core/TextField';
import DialogContent from "@material-ui/core/DialogContent";

import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
}));


function FullScreenDialog(props) {
  const {  anyComponent = false, title, type } = props;
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSubmitActivity = (e) => {
    e.preventDefault();
    console.log('SUBMIT ACTIVITY')
  }

  const handleSubmitSkill = e => {
    e.preventDefault();
    console.log('SUBMIT SKILL')
  }

  const renderForm = (type = 'activity') => {
    let result;
    if(type === 'activity'){
      result = <React.Fragment>
        <TextField className="mb-3 mt-3"
          label="Activity name"
          type="text"
          fullWidth
          required
        />
        <TextField className="mb-3"
          label="Total Hours"
          type="number"
          fullWidth
          required
        />
      </React.Fragment>
    } else {
      result = <React.Fragment>
        <TextField className="mb-3 mt-3"
          label="Skill name"
          type="text"
          fullWidth
          required
        />
        <TextField className="mb-3"
          label="Target"
          type="number"
          fullWidth
          required
        />
      </React.Fragment>
    }
    return result;
  }
  return (
    <React.Fragment>
      {anyComponent ? 
        <Fab style={{ float: 'right' }} color="primary" onClick={handleClickOpen}>
          <AddIcon />
        </Fab> : 
        <IconButton variant="outlined" color="inherit" onClick={handleClickOpen}>
          <AddIcon />
        </IconButton>
      }
      <Dialog fullScreen open={open} onClose={handleClose}>
        <form onSubmit={type === 'activity' ? handleSubmitActivity : handleSubmitSkill}>
          <AppBar className={classes.appBar}>
            <Toolbar>
              <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
                <CloseIcon />
              </IconButton>
              <Typography variant="h6" className={classes.title}>
                {title}
              </Typography>
              <Button autoFocus color="inherit" type="submit">
                save
            </Button>
            </Toolbar>
          </AppBar>
          <DialogContent>
            {renderForm(type)}
          </DialogContent>
        </form>
      </Dialog>
    </React.Fragment>
  );
}

export default FullScreenDialog;