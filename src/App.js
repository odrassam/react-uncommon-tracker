import React, { useEffect } from 'react';
import { createBrowserHistory } from "history";
import { Router, Route, Switch } from "react-router-dom";
import { connect } from 'react-redux';
import { checkLogin } from './redux/auth/action';
import { setUsersTypeSkill } from "./redux/general/action";
import withWidth from "@material-ui/core/withWidth";
import firebase from './firebase'
import 'firebase/firestore';
import WebUI from './pages/web';
import MobileUI from './pages/mobile'

const auth = firebase.auth();
const googleProvider = new firebase.auth.GoogleAuthProvider();
googleProvider.setCustomParameters({ prompt: "select_account" });
 
const App = ({ checkLogin, setUsersTypeSkill, width }) => {
  useEffect(() => {
    let loadingStatus = true;
    const timeOutReload = () => {
      setTimeout(() => {
        window.location.reload();
      }, 5000);
    }
    const checkStatus = uid => {
      setUsersTypeSkill(uid, null)
        .then(payload => {
          if (payload.length > 0) loadingStatus = false;
          checkLogin({ isLoading: loadingStatus, currentUser: uid });
        })
        .catch(e => timeOutReload());
    };
    auth.onAuthStateChanged(user => {
      if (!user) {
        auth
          .signInWithRedirect(googleProvider)
          .then(res => {
            checkStatus(res.user.uid);
          })
          .catch(e => timeOutReload());
      } else {
        checkStatus(user.uid);
      }
    });
  }, [checkLogin, setUsersTypeSkill]);

  const renderWidth = () => {
    if(width === 'xs'){
      return MobileUI
    } else return WebUI
  }

  return (
    <Router history={createBrowserHistory()}>
      <Switch>
        <Route path="/" exact component={renderWidth()} />
        {/* <Route path="/mobile" exact component={MobileUI} /> */}
      </Switch>
    </Router>
  );
};

export default connect(null, { checkLogin, setUsersTypeSkill })(withWidth()(App));